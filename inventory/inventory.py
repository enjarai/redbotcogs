import discord
import asyncio
import calendar
from datetime import timedelta
from redbot.core import commands, checks, bank
from redbot.core.bot import Red
from redbot.core.config import Config

from .itemindex import Item, ItemIndex
from .registeritems import define


from redbot.core.i18n import Translator
from redbot.core.utils.chat_formatting import humanize_number


effect_emojis = {
    "dice": "<:dice:632295947552030741>",
    "uno": "<:unoshield:720992427216863302>",
    "vault": "<:vault:699266653791322172>",
    "none": "⭕"
}
PAYDAY_AMOUNT = 500
PAYDAY_TIME = timedelta(days=1).total_seconds()


class Inventory(commands.Cog):
    """Capitalism"""

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=12132,
            force_registration=True,
        )
        default_user = {
            "items": [],
            "effects": {
                "vault": 0,
                "uno": 0
            },
            "quests": [],
            "last_payday": None
        }
        self.config.register_user(**default_user)
        self.index = define()

    @commands.command()
    async def balance(self, ctx: commands.Context, user: discord.Member = None):
        """Show the user's account balance.
        Example:
            - `[p]balance`
            - `[p]balance @Twentysix`
        **Arguments**
        - `<user>` The user to check the balance of. If omitted, defaults to your own balance.
        """
        if user is None:
            user = ctx.author

        bal = await bank.get_balance(user)
        currency = await bank.get_currency_name(ctx.guild)
        effects = await self.config.user(user).effects()

        vaults_amount = effects["vault"]
        uno = effects["uno"]

        vaults = []
        for i in range(vaults_amount):
            vaults.append(effect_emojis["vault"])
        for i in range(3 - vaults_amount):
            vaults.append(effect_emojis["none"])
        if uno:
            vaults.append("(" + effect_emojis["uno"] + ")")
        vaults = " ".join(vaults)

        embed = discord.Embed(title="Status:", description=f"{humanize_number(bal)} {currency}\n**Active vaults:**\n{vaults}", colour=await ctx.embed_colour())
        embed.set_author(name=user.display_name, icon_url=user.avatar_url)
        await ctx.send(embed=embed)

    @commands.command()
    async def inventory(self, ctx, user: discord.Member = None):
        """Show the items you or another person owns"""
        if not user:
            user = ctx.author

        embed = discord.Embed(title="Inventory:", colour=await ctx.embed_colour())
        embed.set_author(name=user.name, icon_url=user.avatar_url)
        inv = await self.config.user(user).items()
        if inv == []:
            embed.title = "Inventory empty..."
        else:
            for item in inv:
                itemobj = self.index.get_by_id(item[0])
                embed.add_field(name=itemobj.emoji, value=f"""{item[1]}x {itemobj.name}""", inline=True)
        await ctx.send(embed=embed)

    @commands.command()
    async def payday(self, ctx):
        """Recieve your daily injection of cash money"""
        user = ctx.author
        last_payday = await self.config.user(user).last_payday()
        current_time = calendar.timegm(ctx.message.created_at.utctimetuple())
        item = self.index.get_by_id(1)

        if ( not last_payday ) or last_payday + PAYDAY_TIME < current_time:
            await bank.deposit_credits(user, PAYDAY_AMOUNT)
            await self.give_item(user, item)
            await self.config.user(user).last_payday.set(current_time)
            await ctx.send(
                """{author.mention} Here, take some {currency} and loot. Enjoy!
(+{amount} {currency})
(+1 {lootbox})

You currently have {new_balance} {currency}.

You are currently #{pos} on the global leaderboard!
                """.format(
                    author=user,
                    currency=await bank.get_currency_name(ctx.guild),
                    amount=PAYDAY_AMOUNT,
                    lootbox=item,
                    new_balance=await bank.get_balance(user),
                    pos=await bank.get_leaderboard_position(user)
                )
            )
        else:
            time = self.display_time(last_payday + PAYDAY_TIME - current_time)
            await ctx.send("{author.mention}: Too soon. For your next payday you have to wait {time}.".format(author=user,time=time))

    @commands.command()
    @checks.is_owner()
    async def give(self, ctx, itemname, amount: int = 1, user: discord.Member = None):
        """Give item to user"""
        if not user:
            user = ctx.author

        item = self.index.get_by_alias(itemname)
        await self.give_item(user, item, amount)
        await ctx.send(f"Gave {user.name} {amount}x {item.emoji} {item.name}")

    @commands.command()
    @checks.is_owner()
    async def take(self, ctx, itemname, amount: int = 1, user: discord.Member = None):
        """Remove item from user"""
        if not user:
            user = ctx.author

        item = self.index.get_by_alias(itemname)
        await self.take_item(user, item, amount)
        await ctx.send(f"Took {amount}x {item.emoji} {item.name} from {user.name}")

    @commands.command(aliases=["buy"])
    async def shop(self, ctx, buythis = None, amount = 1):
        """Buy items with coins"""
        currencyname = await bank.get_currency_name(ctx.channel.guild)
        if not buythis:
            embed = discord.Embed(title="For sale:", colour=await ctx.embed_colour())
            embed.set_author(name=" ", icon_url=self.bot.user.avatar_url)
            for item in self.index.items:
                if item.buy and item.description:
                    embed.add_field(name=f"**{str(item)}** - {item.buy} {currencyname}", value=item.description, inline=False)
            await ctx.send(embed=embed)
            return

        if amount < 1:
            await ctx.send("Nice try")
            return

        item = self.index.get_by_alias(buythis)
        if not item or not item.buy:
            await ctx.send("I don't sell that")
            return

        if await bank.can_spend(ctx.author, item.buy * amount):
            await ctx.send(f"{ctx.author.mention}: you bought {amount}x {str(item)} for {item.buy * amount} {currencyname}")
            await self.give_item(ctx.author, item, amount)
            await bank.withdraw_credits(ctx.author, item.buy * amount)
        else:
            await ctx.send("You dont have enough money for that")

    @commands.command()
    async def sell(self, ctx, sellthis = None, amount = 1):
        """Sell items for coins"""
        currencyname = await bank.get_currency_name(ctx.channel.guild)
        if amount < 1:
            await ctx.send("Nice try")
            return

        if not sellthis:
            await ctx.send("What would you like to sell?")
            return

        item = self.index.get_by_alias(sellthis)

        if item.id == 14:
            await ctx.send("How could you ever sell this? You can't.")
            return

        if not item or not item.sell:
            await ctx.send("I don't buy that")
            return

        if not await self.has_item(ctx.author, item, amount):
            await ctx.send("You dont have that")
            return

        await ctx.send(f"{ctx.author.mention}: you sold {amount}x {str(item)} for {item.sell * amount} {currencyname}")
        await bank.deposit_credits(ctx.author, item.sell * amount)
        await self.take_item(ctx.author, item, amount)

    @commands.command(aliases=["open"])
    async def use(self, ctx, *args):
        """Do something with your random crap"""
        if not args:
            await ctx.send(f"Name the item that you want to use:\n`{ctx.clean_prefix}open lootbox")
            return

        item = self.index.get_by_alias(args[0])
        if not item or not item.use:
            await ctx.send("I dont know that item")
            return

        if not await self.has_item(ctx.author, item):
            await ctx.send("You dont have that item")
            return

        if item.useargs == "m":
            if len(args) == 1:
                await ctx.send("Please mention the user you want to use this item on")
                # TODO: add follow up detection
                return
            else:
                if ctx.message.mentions:
                    member = ctx.message.mentions[0]
                else:
                    member = ctx.guild.get_member_named(args[1])
                if not member:
                    await ctx.send("I dont know that user")
                    return

            rmitem = await item.use(self, ctx, member)
        elif item.useargs == "i":
            if len(args) == 1:
                await ctx.send("This item needs a number")
                return
            try:
                amount = int(args[1])
            except ValueError:
                await ctx.send("Thats not number")
                return

            rmitem = await item.use(self, ctx, amount)
        else:
            rmitem = await item.use(self, ctx)

        if rmitem:
            await self.take_item(ctx.author, item)

    @commands.command()
    async def iteminfo(self, ctx, item):
        """View the description of an item"""
        item = self.index.get_by_alias(item)

        if item:
            await ctx.send(item.longdesc)
        else:
            await ctx.send("I dont know that item")

    @commands.command(aliases=["quests"])
    async def quest(self, ctx):
        """View your current quests"""
        user = ctx.author
        current_time = calendar.timegm(ctx.message.created_at.utctimetuple())

        embed = discord.Embed(colour=await ctx.embed_colour())
        embed.set_author(name=user.name + "'s quests:", icon_url=user.avatar_url)
        quests = await self.config.user(user).quests()
        if quests == []:
            embed.title = "No quests..."
        else:
            for i in quests:
                item = self.index.get_by_id(i["reward"][0])
                timedelta = i["expires"] - current_time
                embed.add_field(name=i["description"], value=f"Reward: {i['reward'][1]}x {str(item)}\nProgress: {i['progress']}/{i['amount']}\nTime left: {self.display_time(timedelta)}", inline=False)
        await ctx.send(embed=embed)

    @commands.command()
    @checks.is_owner()
    async def give_quest(self, ctx, description, reward):
        user = ctx.author
        current_time = calendar.timegm(ctx.message.created_at.utctimetuple())

        async with self.config.user(user).quests() as quests:
            quests.append({
                "description": description,
                "reward": (self.index.get_by_alias(reward).id, 1),
                "progress": 0,
                "amount": 0,
                "expires": 100 + current_time
            })
        await ctx.send("hmpf done")

    async def give_item(self, user: discord.Member, item: Item, amount: int = 1):
        async with self.config.user(user).items() as inv:
            for i in range(len(inv)):
                if inv[i][0] == item.id:
                    inv[i][1] += amount
                    return

            inv.append([
                item.id, amount
            ])

    async def take_item(self, user: discord.Member, item: Item, amount: int = 1):
        async with self.config.user(user).items() as inv:
            for i in range(len(inv)):
                if inv[i][0] == item.id:
                    inv[i][1] -= amount
                    if not inv[i][1] < 1:
                        return
                    itempos = i
                    break
            del inv[itempos]

    async def has_item(self, user: discord.Member, item: Item, amount: int = 1):
        async with self.config.user(user).items() as inv:
            for i in inv:
                if i[0] == item.id and i[1] >= amount:
                    return True
        return False

    # What would I ever do without stackoverflow?
    @staticmethod
    def display_time(seconds, granularity=2):
        intervals = (  # Source: http://stackoverflow.com/a/24542445
            ("weeks", 604800),  # 60 * 60 * 24 * 7
            ("days", 86400),  # 60 * 60 * 24
            ("hours", 3600),  # 60 * 60
            ("minutes", 60),
            ("seconds", 1),
        )

        result = []

        for name, count in intervals:
            value = seconds // count
            if value:
                seconds -= value * count
                if value == 1:
                    name = name.rstrip("s")
                result.append("{} {}".format(int(value), name))
        return ", ".join(result[:granularity])
