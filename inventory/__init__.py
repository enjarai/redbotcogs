from redbot.core.bot import Red

from .inventory import Inventory

async def setup(bot: Red) -> None:
    bot.add_cog(Inventory(bot))