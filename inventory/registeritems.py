import discord
import random
from redbot.core import bank
from redbot.core.bot import Red
from redbot.core.config import Config
from .itemindex import Item, ItemIndex
import subprocess


def define():
    index = ItemIndex()

    index.add(
        name="Coins",
        emoji=":coin:",
        lootboxmax=400,
        lootboxweight=2000,
        genaliases=False
    )


    async def item_lootbox(self, ctx):
        """This is a loot box, in case you hadn't guessed.

        Here are some drop rates i guess:
        """
        embed = discord.Embed(title="Loot Box opened!", description="You got:", colour=await ctx.embed_colour())
        embed.set_author(name=ctx.author.name, icon_url=ctx.author.avatar_url)
        for i in range(3):
            weights = []
            for item in self.index.items:
                weights.append(item.lootboxweight)
            addthis = random.choices(index.items, weights)[0]
            amount = random.randint(1, addthis.lootboxmax)
            if addthis.id == 0:
                embed.add_field(name="<:coin:632592319245451286>", value=f"{amount} Points", inline=True)
                await bank.deposit_credits(ctx.author, amount)
            else:
                embed.add_field(name=addthis.emoji, value=f"{amount}x {addthis.name}", inline=True)
                await self.give_item(ctx.author, addthis, amount)
        await ctx.send(embed=embed)
        return True

    index.add(
        use=item_lootbox,
        name="Loot Box",
        emoji="<:lootbox:632286669592199217>",
        aliases=[],
        description="Some say it's gambling, so imma add it while it's legal...",
        buy=500,
        sell=300,
        dicenum=3,
        diceweight=10
    )


    async def item_dice(self, ctx):
        """A magical dice that bestows upon you what you need most.

        *\\* totally not inspired by Botania*
        """
        weights = []
        for item in self.index.items:
            weights.append(item.diceweight)
        result = random.choices(index.items, weights)[0]
        await ctx.send(ctx.author.mention + f": You roll the Dice of Destiny, it lands on a {result.dicenum} and transforms into a {str(result)}!")
        await self.give_item(ctx.author, result, 1)
        return True

    index.add(
        use=item_dice,
        name="Dice of Destiny",
        emoji="<:dice:851037279576915968>",
        aliases=[],
        description="A magical dice that bestows upon you what you need most.",
        lootboxmax=3,
        lootboxweight=1500,
        buy=150,
        sell=80,
        dicenum=1,
        diceweight=10
    )


    async def item_spambot(self, ctx, member):
        """You all know that feeling when your friends are online,
        but they never respond to your messages.
        This item is the solution to that problem!

        Using this item on another user will spam the chat with their pings.
        Now available for only $19.99! *terms and conditions apply*

        *\\* please dont abuse :)*
        """
        for i in range(5):
            await ctx.send(member.mention + ": get spammed nerd")
        return True

    index.add(
        use=item_spambot,
        name="Spambot",
        emoji="<:spambot:632466831063646221>",
        aliases=["bot", "spam"],
        description="Spams the crap out of your target",
        lootboxmax=3,
        lootboxweight=2000,
        buy=80,
        sell=30,
        useargs="m",
        dicenum=5,
        diceweight=10
    )


    async def item_mask(self, ctx, member):
        """You filthy thief!
        Stealing up to 300 points from your friends using this item is not nice.
        I sure do hope they have vaults active to stop you...

        *\\* Elysium Corp is not responsible for any bans as the result of robbing admins*
        """
        user = ctx.author
        target = member

        user_balance = await bank.get_balance(target)
        if user_balance >= 300:
            amount = random.randint(40, 300)
        elif user_balance < 50:
            amount = 0
        else:
            amount = random.randint(40, user_balance)

        if not amount:
            await ctx.send(user.mention + f": You cant rob {target.mention}! They're way too poor, thats pathetic... *shakes head disapprovingly*")
            return False

        async with self.config.user(target).effects() as target_effects:
            if target_effects["uno"]:
                amount = -(amount * 2)
                await ctx.send(user.mention + f": You robbed {target.mention}, but they had an uno reverse shield active! you lost `{amount}` points!")
                target_effects["uno"] -= 1
            elif target_effects["vault"]:
                amount = 0
                await ctx.send(user.mention + f": You robbed {target.mention}, but they had a vault active and you lost your mask!")
                target_effects["vault"] -= 1
            else:
                await ctx.send(user.mention + f": You robbed {target.mention}, you managed to get away with `{amount}` points!")

        if amount:
            bal = await bank.get_balance(target)
            await bank.set_balance(target, bal - amount)
            bal = await bank.get_balance(user)
            await bank.set_balance(user, bal + amount)
        return True

    index.add(
        use=item_mask,
        name="Robbers Mask",
        emoji="<:balaclava:632658938437042212>",
        aliases=[],
        description="Use this to steal some points from your buddies, i'm sure they won't hate you...",
        lootboxmax=1,
        lootboxweight=400,
        buy=200,
        sell=40,
        useargs="m",
        dicenum=2,
        diceweight=10
    )


    async def item_bread(ctx):
        """Okay seriously thats gross, its almost like a new lifeform has evolved here!
        Wait you wanna eat this? Would you even survive that?!

        Actually eating it seems to just remove 10 points.
        But it doesn't stop you from eating it is you have no points left, that might be worth investigating...

        *\\* this might get really useful later on, once i get around to adding the thing*
        """
        await ctx.send(ctx.author.mention + ": You ate the Moldy Bread, why the fuck would you do that? *backs away slowly*\nU got -10 <:coin:632592319245451286> cus thats just nasty")
        db.update_bal(ctx.author.id, -10)
        return True

    index.add(
        use=item_bread,
        name="Moldy Bread",
        emoji="<:moldybread:632921575649443909>",
        aliases=[],
        description="Why would you keep this?",
        lootboxmax=1,
        lootboxweight=100,
        buy=200,
        sell=50
    )


    async def item_fortune(self, ctx):
        """"A fortune cookie is a crisp and sugary cookie usually made from flour, sugar, vanilla, and sesame seed oil with a piece of paper inside, a "fortune", on which is an aphorism, or a vague prophecy."
        *(from https://en.wikipedia.org/wiki/Fortune_cookie)*

        Well Wikipedia isn't wrong, but there might be more to it than that...

        *\\* expect more here in the future*
        """
        await ctx.send(ctx.author.mention + f""": You cracked open the cookie, the little piece of paper inside says:\n```{subprocess.check_output(["/usr/games/fortune"]).decode("utf-8")}```""")
        if random.randint(1, 10) == 1:
            cash = random.randint(50, 300)
            await ctx.send(ctx.author.mention + f""": There were also {cash} <:coin:632592319245451286> hidden inside!""")
            await bank.deposit_credits(ctx.author, cash)
            # TODO: add item drops rarely, better than lootbox?
        return True

    index.add(
        use=item_fortune,
        name="Fortune Cookie",
        emoji="<:fortunecookie:633286682195525653>",
        aliases=[],
        description="Tells you your fortune i guess, sometimes has something hidden inside tho",
        lootboxmax=3,
        lootboxweight=1500,
        buy=80,
        sell=5
    )


    async def item_nuke(self, ctx, member):
        """Pretty cool right, having a nuke in your pocket?
        Using this is more effective than just putting on a mask, just like real life!

        This is more effective at stealing points, but there's also some collateral damage.

        *\\* tbh, it is not that good, thats why it's on discount :/*
        """
        user = ctx.author
        target = member
        half = False

        user_balance = await bank.get_balance(target)
        if user_balance >= 500:
            amount = random.randint(0, 500)
        elif user_balance < 0:
            amount = -random.randint(0, -user_balance)
        else:
            amount = random.randint(0, user_balance)

        async with self.config.user(target).effects() as target_effects:
            if target_effects["uno"]:
                amount = -(amount * 2)
                await ctx.send(user.mention + f": You yeeted a nuke at {target.mention}, but they had an uno reverse shield active! you lost `{amount}` points!")
                target_effects["uno"] -= 1
            elif target_effects["vault"]:
                amount = 0
                await ctx.send(user.mention + f": You yeeted a nuke at {target.mention}, but they had a vault active!")
                target_effects["vault"] -= 1
            else:
                await ctx.send(user.mention + f": You yeeted a nuke at {target.mention}, you stole `{amount}` points, but half of them were destroyed!")
                half = True

        if amount:
            bal = await bank.get_balance(target)
            await bank.set_balance(target, bal - amount)
            if half:
                amount = int(amount / 2)
            bal = await bank.get_balance(user)
            await bank.set_balance(user, bal + amount)
        return True

    index.add(
        use=item_nuke,
        name="Nuke",
        emoji="<:nuke:671718044078440448>",
        aliases=[],
        description="Steals points from pals but destroys half of 'em",
        lootboxmax=1,
        lootboxweight=200,
        buy=200,
        sell=100,
        useargs="m",
        dicenum=4,
        diceweight=10
    )


    async def item_nuke2(ctx, member):
        """Fuck your friends in the ass today with the new NUKE 2: ELECTRIC BOOGALOO,
        as opposed to the old nuke this one is acually better than a mask! *wow*

        This is exactly the same as the normal nuke, but more destructive.

        *\\* where the normal nuke destroyed one city, this one destroys about 5 at least*
        """
        memscore = db.get_bal(member.id)
        if memscore >= 1000:
            amount = random.randint(0, 1000)
        elif memscore < 0:
            amount = -random.randint(0, -memscore)
        elif memscore < 1000:
            amount = random.randint(int(memscore * 0.4), memscore)
        memeff = db.get_eff(member.id)
        if "uno" in memeff:
            amount = amount * 2
            db.update_bal(member.id, int(amount / 2))
            db.update_bal(ctx.author.id, -amount)
            await ctx.send(ctx.author.mention + f": You yeeted a nuke 2: electric boogaloo at {member.mention}, but they had an uno reverse card active! you lost `{amount}` points, and half of them were destroyed!")
            db.rem_eff(member.id, "uno")
        elif "vault" in memeff:
            await ctx.send(ctx.author.mention + f": You yeeted a nuke 2: electric boogaloo at {member.mention}, but they had a vault active!")
            db.rem_eff(member.id, "vault")
        else:
            db.update_bal(member.id, -amount)
            db.update_bal(ctx.author.id, int(amount / 2))
            db.log(member.id, "steal", ctx.author.id, amount)
            await ctx.send(ctx.author.mention + f": You yeeted a nuke 2: electric boogaloo at {member.mention}, you stole `{amount}` points, but half of them were destroyed!")
        return True

    index.add(
        use=item_nuke2,
        name="Nuke 2: Electric Boogaloo",
        emoji="<:nuke2:698057397574303784>",
        aliases=["nuke2"],
        description="The cooler daniel, no discount here",
    #    lootboxmax=1,
    #    lootboxweight=80,
    #    buy=3000,
        sell=1000,
        useargs="m"
    )


    async def item_unoshield(self, ctx):
        """Okay so we're playing uno now apparently,
        this acually seems pretty useful though.

        The reverse shield can reverse a single rob or nuke.
        After that it just disappears. like, \*poof\*, and its gone.

        *\\* oh yeah, and it doubles the amount robbed. so thats nice...*
        """
        user = ctx.author
        async with self.config.user(user).effects() as effects:

            if effects["uno"] >= 1:
                await ctx.send(user.mention + f""": You already have a reverse shield active""")
                return False

            effects["uno"] += 1
            await ctx.send(user.mention + f""": Used item""")
            return True

    index.add(
        use=item_unoshield,
        name="Reverse Shield",
        emoji="<:unoshield:720992427216863302>",
        aliases=["unoshield", "reverseshield", "shield"],
        description="Use this to ward off those pesky thieves once and for all",
        lootboxmax=1,
        lootboxweight=40,
        buy=400,
        sell=300,
        genaliases=False,
        dicenum=6,
        diceweight=10
    )


    async def item_vault(self, ctx):
        """This miraculous item seems to be able to stop an entire nuke!
        It does also break from a simple robbery though...

        Use a vault to protect your points from robberies and nukes.
        After activating a vault it will protect your balance from a single attack!
        You can have 3 vaults active at once, plus a single uno card.

        *\\* It's not as useful since the introduction of the lockpick...*
        """
        user = ctx.author
        async with self.config.user(user).effects() as effects:

            if effects["vault"] >= 3:
                await ctx.send(user.mention + f""": You already have 3 vaults active""")
                return False

            effects["vault"] += 1
            await ctx.send(user.mention + f""": Used item, you now have {effects["vault"]} active vaults""")
            return True

    index.add(
        use=item_vault,
        name="Vault",
        emoji="<:vault:699266653791322172>",
        aliases=[],
        description="Protect your precious points, stops one attack each, 3 allowed active at once",
        lootboxmax=1,
        lootboxweight=400,
        buy=100,
        sell=100
    )


    async def item_lockpick(ctx, self, target):
        """Wasting masks and nukes on removing vaults is not very nice is it?

        This item can remove a single active vault from someone's balance.
        Once there are no vaults left it's pretty much useless...

        *\\* okay so i might have made this a bit too common...*
        """
        user = ctx.author
        async with self.config.user(target).effects() as effects:

            if effects["uno"]:
                await ctx.send(user.mention + f": {target.mention} has a reverse shield in the way")
                return False

            if not effects["vault"]:
                await ctx.send(user.mention + f": {target.mention} has no vaults active")
                return False

            effects["vault"] -= 1
            await ctx.send(user.mention + f""": You cracked one of {target.mention}'s vaults! They have {effects["vaults"]} vaults left.""")
            return True

    index.add(
        use=item_lockpick,
        name="Lockpick",
        emoji="<:lockpick:699275348675657788>",
        aliases=[],
        description="Removes a vault, nothing else",
        lootboxmax=2,
        lootboxweight=400,
        buy=150,
        sell=100,
        useargs="m"
    )

    async def item_rulebook(ctx, member):
        """Wasting masks and nukes on removing ~~vaults~~ **reverse shields** is not very nice is it?

        This item can remove a single active ~~vault~~ **reverse shields** from someone's balance.
        Once there are no ~~vaults~~ **reverse shields** left it's pretty much useless...

        *\\* okay so i might have made this a bit too uncommon...*
        """
        if "uno" in db.get_eff(member.id):
            db.rem_eff(member.id, "uno")
            await ctx.send(ctx.author.mention + f""": You annihilated {member.mention}'s reverse shield!""")
        else:
            await ctx.send(ctx.author.mention + f""": {member.mention} has no reverse shield active""")
            return False
        return True

    index.add(
        use=item_rulebook,
        name="Uno Rulebook",
        emoji="<:rulebook:718503942153044081>",
        aliases=["rulebook", "rules", "unorulebook", "book", "rule"],
        description="Another counter item",
    #    lootboxmax=1,
    #    lootboxweight=20,
    #    buy=14000,
        sell=1000,
        useargs="m",
        genaliases=False
    )


    async def item_unocard(ctx):
        """This uno card seems different...

        If this item is used within one hour of a rob/nuke the effect can be reversed, making the attacker lose points!
        This is *not* limited to only the most recent rob/nuke.

        WIP, TODO, PLSFIX i need to get the bot to keep a record of all robs ever and i dont want too because its effort but i need it to make this item work so im in pain now help why do yall want me to make a working bot im not a programmer this thing is garbage

        *\\* this seemed a bit more appropriate for the uno card*
        """
        dbreturn = db.latest_log(ctx.author.id, "steal")
        try:
            affected, amount = dbreturn
        except TypeError:
            await ctx.send(ctx.author.mention + f""": You have no recent robs to reverse""")
            return False

        db.update_bal(affected, -amount * 2)
        db.update_bal(ctx.author.id, amount * 2)
        await ctx.send(ctx.author.mention + f""": You reversed {client.get_user(affected).mention}'s rob of {amount} {index.get_by_id(0).emoji}!""")

        return True

    index.add(
        use=item_unocard,
        name="Uno Reverse Card",
        emoji="<:unoreverse:699194687646597130>",
        aliases=["unoreverse"],
        description="Reverse that shit!",
        lootboxmax=1,
        lootboxweight=300,
        buy=350,
        sell=200
    )

    index.add(
        name="Tiny Potato",
        emoji="<:tinypotato:851046006346088449>",
        aliases=[],
        description="Neat is a mod by Vaskii.",
        dicenum=7,
        diceweight=1
    )

    return index
