from redbot.core.bot import Red

from .pressf import PressF

async def setup(bot: Red) -> None:
    bot.add_cog(PressF(bot))