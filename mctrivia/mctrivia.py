import discord
import random
import json
import glob
import pathlib
import asyncio
from redbot.core import commands, checks
from redbot.core.bot import Red
from redbot.core.config import Config
from .session import TriviaSession

class McTrivia(commands.Cog):
    """Port van het trivia systeem van Suicidal Memer"""

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.trivia_sessions = []
        self.config = Config.get_conf(
            self,
            identifier=12131,
            force_registration=True,
        )
        default_guild = {
            "channel": 0,
            "minwait": 300,
            "maxwait": 600,
            "multiplier": 10
        }
        self.config.register_guild(**default_guild)
        qdir = pathlib.Path(__file__).parent.resolve() / "data/questions"
        qfiles = qdir.glob("*.json")
        self.questions = []
        for i in qfiles:
            with open(i, "r") as f:
                q = json.load(f)
            self.questions.append(q)

        bot.loop.create_task(self._start_trivias())

    async def _start_trivias(self):
        await self.bot.wait_until_ready()
        for guild in self.bot.guilds:
            if await self.config.guild(guild).channel():
                settings = await self.config.guild(guild).all()
                session = TriviaSession.start(self.bot, self.questions, settings)
                self.trivia_sessions.append(session)

    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    @commands.group()
    async def mctriviaset(self, ctx: commands.Context):
        """Manage McTrivia settings."""
        pass

    @mctriviaset.command()
    async def channel(self, ctx: commands.Context, channel=0):
        """Set channel id for trivia."""
        if (channel > 999999999999999999 or channel < 99999999999999999) and channel != 0:
            await ctx.send("Channel id is of incorrect format")
            return

        await self.config.guild(ctx.guild).channel.set(channel)
        if channel:
            await ctx.send("Channel id is now {num}\nReload cog to apply".format(num=channel))
        else:
            await ctx.send("Channel id has been disabled\nReload cog to apply")

    def cog_unload(self):
        for session in self.trivia_sessions:
            session.stop()
        