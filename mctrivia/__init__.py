from redbot.core.bot import Red

from .mctrivia import McTrivia

async def setup(bot: Red) -> None:
    bot.add_cog(McTrivia(bot))