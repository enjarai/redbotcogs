"""Module to manage trivia sessions."""
import asyncio
import time
import random
import emoji
from collections import Counter
import discord
from redbot.core import bank, errors
from redbot.core.i18n import Translator
from redbot.core.utils.chat_formatting import box, bold, humanize_list, humanize_number
from redbot.core.utils.common_filters import normalize_smartquotes


class TriviaSession:
    """Class to run a session of trivia with the user.

    To run the trivia session immediately, use `TriviaSession.start` instead of
    instantiating directly.

    Attributes
    ----------
    ctx : `commands.Context`
        Context object from which this session will be run.
        This object assumes the session was started in `ctx.channel`
        by `ctx.author`.
    question_list : `dict`
        A list of tuples mapping questions (`str`) to answers (`list` of
        `str`).
    settings : `dict`
        Settings for the trivia session, with values for the following:
         - ``max_score`` (`int`)
         - ``delay`` (`float`)
         - ``timeout`` (`float`)
         - ``reveal_answer`` (`bool`)
         - ``bot_plays`` (`bool`)
         - ``allow_override`` (`bool`)
         - ``payout_multiplier`` (`float`)
    scores : `collections.Counter`
        A counter with the players as keys, and their scores as values. The
        players are of type `discord.Member`.
    count : `int`
        The number of questions which have been asked.

    """

    def __init__(self, bot, questions: list, settings: dict):
        self.bot = bot
        self.channel = bot.get_channel(settings["channel"])
        self.questions = questions
        self.settings = settings
        self.count = 0
        self._last_response = time.time()
        self._task = None

    @classmethod
    def start(cls, bot, questions, settings):
        """Create and start a trivia session.

        This allows the session to manage the running and cancellation of its
        own tasks.

        Parameters
        ----------
        ctx : `commands.Context`
            Same as `TriviaSession.ctx`
        question_list : `dict`
            Same as `TriviaSession.question_list`
        settings : `dict`
            Same as `TriviaSession.settings`

        Returns
        -------
        TriviaSession
            The new trivia session being run.

        """
        session = cls(bot, questions, settings)
        loop = bot.loop
        session._task = loop.create_task(session.run())
        session._task.add_done_callback(session._error_handler)
        return session

    def _error_handler(self, fut):
        """Catches errors in the session task."""
        try:
            fut.result()
        except asyncio.CancelledError:
            pass
        except Exception as exc:
            asyncio.create_task(
                self.channel.send(
                    _(
                        "An unexpected error occurred in the trivia session.\nCheck your console or logs for details."
                    )
                )
            )
            self.stop()

    async def run(self):
        """Run the trivia session.

        In order for the trivia session to be stopped correctly, this should
        only be called internally by `TriviaSession.start`.
        """
        embed=discord.Embed(description="Starting Trivia...")
        await self.channel.send(embed=embed)
        while True:
            await asyncio.sleep(random.randint(self.settings["minwait"], self.settings["maxwait"]))
            q = random.choice(self.questions)
            async with self.channel.typing():
                await asyncio.sleep(3)
            self.count += 1
            qstr = q["text"]
            for k, i in q["options"].items():
                qstr += "\n" + f"{k}: {i}"
            embed = discord.Embed(title="Minecraft Trivia Question:", description=qstr, color=0x51e443)
            embed.set_thumbnail(url="https://icons.iconarchive.com/icons/papirus-team/papirus-apps/512/minecraft-icon.png")
            embedmsg = await self.channel.send(embed=embed)
            for k, i in q["options"].items():
                await embedmsg.add_reaction(emoji.emojize(k, use_aliases=True))
            tries = 0
            while True:
                payload = await self.bot.wait_for("raw_reaction_add")
                if not payload:
                    embed=discord.Embed(description="Question expired...")
                    await self.channel.send(embed=embed)
                    break
                guild = self.bot.get_guild(payload.guild_id)
                channel = guild.get_channel(payload.channel_id)
                member = guild.get_member(payload.user_id)
                early_exit = channel != self.channel or member == self.channel.guild.me
                if not early_exit:
                    rawemoji = payload.emoji
                    message = await channel.fetch_message(payload.message_id)
                    await message.remove_reaction(rawemoji, member)
                    if emoji.demojize(str(rawemoji), use_aliases=True) in q["correct"]:
                        points = (10 - (2 * tries)) * self.settings["multiplier"]
                        embed=discord.Embed(description="🟢 {} has earned {} {}!".format(member, points, await bank.get_currency_name(self.channel.guild)))
                        await self.channel.send(embed=embed)
                        try:
                            await bank.deposit_credits(member, points)
                        except errors.BalanceTooHigh as e:
                            await bank.set_balance(member, e.max_balance)                        
                        break
                    else:
                        if tries < 4:
                            tries += 1
                        embed=discord.Embed(description="🔴 {} has answered wrongly!".format(member))
                        await channel.send(embed=embed)

    def stop(self):
        """Stop the trivia session, without showing scores."""
        self._task.cancel()
        embed=discord.Embed(description="Stopping Trivia...")
        loop = self.bot.loop
        loop.create_task(self.channel.send(embed=embed))