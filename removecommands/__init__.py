from redbot.core.bot import Red

from .removecommands import RemoveCommands

async def setup(bot: Red) -> None:
    bot.add_cog(RemoveCommands(bot))