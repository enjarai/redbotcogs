import discord
from redbot.core import commands, checks
from redbot.core.bot import Red
from redbot.core.config import Config

remove = ["leave", "licenseinfo", "mydata", "bank balance"]

class RemoveCommands(commands.Cog):
    """Get em outta ere"""

    def __init__(self, bot: Red) -> None:
        for comm in remove:
            bot.remove_command(comm)
